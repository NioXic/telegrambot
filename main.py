import functools

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup, KeyboardButton, \
    ReplyKeyboardRemove, CallbackQuery, bot, update
import logging
from datetime import datetime, timedelta
from telegram import Update, Dice, MessageEntity
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext, \
    CallbackQueryHandler
import schedule
import time

TOKEN = '5623938137:AAGS1-EGpqC8YLEXU38rJBY5LoPG_g9ZSzM'

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

last_max_value_user = {}
last_max_value_user_state = {}
last_max_value_emoji = {}
last_max_value_time = {}
last_emoji_game = {}
last_user = {}
emergency_user = {}
leaderboard = {}

user_last_game_time = {}
user_max_value_count = {}

GROUP_CHAT_IDS = []


def reset_emergency(context: CallbackContext) -> None:
    global emergency_user, basketball_wins, football_wins, jackpot_wins, bowling_wins, dart_wins, dice_wins, basketball_rolls, football_rolls, dart_rolls, bowling_rolls, jackpot_rolls, dice_rolls
    for chat in emergency_user.keys():
        chat = emergency_user.get(chat)
        for user, state in chat.items():
            state['used'] = 0
    user_list = []
    for chat_id in leaderboard.keys():
        group = leaderboard.get(chat_id)
        for user_id, user_data in group.items():
            if user_data.get('Dice') is not None:
                dice_data = user_data.get('Dice')
                if dice_data is not None:
                    dice_rolls = dice_data.get('total', 0)
                    dice_wins = dice_data.get('win', 0)
            else:
                dice_rolls = 0
                dice_wins = 0
            if user_data.get('Dart') is not None:
                dart_data = user_data.get('Dart')
                if dart_data is not None:
                    dart_rolls = dart_data.get('total', 0)
                    dart_wins = dart_data.get('win', 0)
            else:
                dart_rolls = 0
                dart_wins = 0
            if user_data.get('Bowling') is not None:
                bowling_data = user_data.get('Bowling')
                if bowling_data is not None:
                    bowling_rolls = bowling_data.get('total', 0)
                    bowling_wins = bowling_data.get('win', 0)
            else:
                bowling_rolls = 0
                bowling_wins = 0
            if user_data.get('Jackpot') is not None:
                jackpot_data = user_data.get('Jackpot')
                if jackpot_data is not None:
                    jackpot_rolls = jackpot_data.get('total', 0)
                    jackpot_wins = jackpot_data.get('win', 0)
            else:
                jackpot_rolls = 0
                jackpot_wins = 0
            if user_data.get('Football') is not None:
                football_data = user_data.get('Football')
                if football_data is not None:
                    football_rolls = football_data.get('total', 0)
                    football_wins = football_data.get('win', 0)
            else:
                football_rolls = 0
                football_wins = 0
            if user_data.get('Basketball') is not None:
                basketball_data = user_data.get('Basketball')
                if basketball_data is not None:
                    basketball_rolls = basketball_data.get('total', 0)
                    basketball_wins = basketball_data.get('win', 0)
            else:
                basketball_rolls = 0
                basketball_wins = 0
            total_points = int(basketball_wins * 5 / 2) + int(dice_wins * 6) + int(
                football_wins * 5 / 3) + int(bowling_wins * 6) + \
                           int(dart_wins * 6) + int(jackpot_wins * 64 / 4)

            total_could_win = int(basketball_rolls * 5 / 2) + int(dice_rolls * 6) + int(
                football_rolls * 5 / 3) + int(bowling_rolls * 6) + \
                              int(dart_rolls * 6) + int(jackpot_rolls * 64 / 4)

            lucky = int((total_points/total_could_win) * 100)

            total_games = user_data.get('total_games', 0)
            user_list.append(
                {'id': user_id, 'name': user_data.get('name', None), 'total_points': total_points,
                 'basketball_games': basketball_rolls, 'basketball_wins': basketball_wins, 'dice_games': dice_rolls, 'dice_wins': dice_wins,
                 'football_games': football_rolls, 'football_wins': football_wins, 'bowling_games': bowling_rolls, 'bowling_wins': bowling_wins,
                 'dart_games': dart_rolls, 'dart_wins': dart_wins,'jackpot_games': jackpot_rolls, 'jackpot_wins': jackpot_wins, 'lucky': lucky,
                 "total": total_games})
        sorted_user_list = sorted(user_list, key=lambda x: x['total_points'], reverse=True)
        message = ""
        for index, user in enumerate(sorted_user_list):
            if index == 0:
                message += f"🥇 {user['name']} ({user['total_points']} points of {user['total']} games) ({user['lucky']}% Lucky Ass)\n"
                message += f"🏀 = {user['basketball_wins']} of {user['basketball_games']}\n"
                message += f"🎯 = {user['dart_wins']} of {user['dart_games']}\n"
                message += f"🎳 = {user['bowling_wins']} of {user['bowling_games']}\n"
                message += f"⚽ = {user['football_wins']} of {user['football_games']}\n"
                message += f"🎰 = {user['jackpot_wins']} of {user['jackpot_games']}\n"
                message += f"🎲 = {user['dice_wins']} of {user['dice_games']}\n\n"
            elif index == 1:
                message += f"🥈 {user['name']} ({user['total_points']} points of {user['total']} games) ({user['lucky']}% Lucky Ass)\n"
                message += f"🏀 = {user['basketball_wins']} of {user['basketball_games']}\n"
                message += f"🎯 = {user['dart_wins']} of {user['dart_games']}\n"
                message += f"🎳 = {user['bowling_wins']} of {user['bowling_games']}\n"
                message += f"⚽ = {user['football_wins']} of {user['football_games']}\n"
                message += f"🎰 = {user['jackpot_wins']} of {user['jackpot_games']}\n"
                message += f"🎲 = {user['dice_wins']} of {user['dice_games']}\n\n"
            elif index == 2:
                message += f"🥉 {user['name']} ({user['total_points']} points of {user['total']} games) ({user['lucky']}% Lucky Ass)\n"
                message += f"🏀 = {user['basketball_wins']} of {user['basketball_games']}\n"
                message += f"🎯 = {user['dart_wins']} of {user['dart_games']}\n"
                message += f"🎳 = {user['bowling_wins']} of {user['bowling_games']}\n"
                message += f"⚽ = {user['football_wins']} of {user['football_games']}\n"
                message += f"🎰 = {user['jackpot_wins']} of {user['jackpot_games']}\n"
                message += f"🎲 = {user['dice_wins']} of {user['dice_games']}\n\n"
            else:
                message += f"🏅 {user['name']} ({user['total_points']} points of {user['total']} games) ({user['lucky']}% Lucky Ass)\n"
                message += f"🏀 = {user['basketball_wins']} of {user['basketball_games']}\n"
                message += f"🎯 = {user['dart_wins']} of {user['dart_games']}\n"
                message += f"🎳 = {user['bowling_wins']} of {user['bowling_games']}\n"
                message += f"⚽ = {user['football_wins']} of {user['football_games']}\n"
                message += f"🎰 = {user['jackpot_wins']} of {user['jackpot_games']}\n"
                message += f"🎲 = {user['dice_wins']} of {user['dice_games']}\n\n"

            # print(f"{rank_symbol} {user_name}: Basketball={basketball_score}, Dart={dart_score}, Total={total_score}")

        context.bot.send_message(chat_id=chat_id, text=message, parse_mode="Markdown")
        leaderboard[chat_id] = None

# return emergency_user


def text_handler(update: Update, context: CallbackContext) -> None:
    global leaderboard, emergency_user, football_wins, basketball_wins, dice_wins, bowling_wins, jackpot_wins, dart_wins, football_rolls, basketball_rolls, bowling_rolls, dice_rolls, dart_rolls, jackpot_rolls
    user_text = update.message.text
    chat_id = update.message.chat.id
    user_id = update.message.from_user.id

    if user_text == '🚑':
        emergency_user[chat_id][user_id]['state'] = True
        # print(emergency_user[chat_id][user_id]['state'])
    if user_text == 'LeaderBoard🏆':
        user_list = []
        group = leaderboard.get(chat_id)
        for user_id, user_data in group.items():
            if user_data.get('Dice') is not None:
                dice_data = user_data.get('Dice')
                if dice_data is not None:
                    dice_rolls = dice_data.get('total', 0)
                    dice_wins = dice_data.get('win', 0)
            else:
                dice_rolls = 0
                dice_wins = 0
            if user_data.get('Dart') is not None:
                dart_data = user_data.get('Dart')
                if dart_data is not None:
                    dart_rolls = dart_data.get('total', 0)
                    dart_wins = dart_data.get('win', 0)
            else:
                dart_rolls = 0
                dart_wins = 0
            if user_data.get('Bowling') is not None:
                bowling_data = user_data.get('Bowling')
                if bowling_data is not None:
                    bowling_rolls = bowling_data.get('total', 0)
                    bowling_wins = bowling_data.get('win', 0)
            else:
                bowling_rolls = 0
                bowling_wins = 0
            if user_data.get('Jackpot') is not None:
                jackpot_data = user_data.get('Jackpot')
                if jackpot_data is not None:
                    jackpot_rolls = jackpot_data.get('total', 0)
                    jackpot_wins = jackpot_data.get('win', 0)
            else:
                jackpot_rolls = 0
                jackpot_wins = 0
            if user_data.get('Football') is not None:
                football_data = user_data.get('Football')
                if football_data is not None:
                    football_rolls = football_data.get('total', 0)
                    football_wins = football_data.get('win', 0)
            else:
                football_rolls = 0
                football_wins = 0
            if user_data.get('Basketball') is not None:
                basketball_data = user_data.get('Basketball')
                if basketball_data is not None:
                    basketball_rolls = basketball_data.get('total', 0)
                    basketball_wins = basketball_data.get('win', 0)
            else:
                basketball_rolls = 0
                basketball_wins = 0
            total_points = int(basketball_wins * 5 / 2) + int(dice_wins * 6) + int(
                football_wins * 5 / 3) + int(bowling_wins * 6) + \
                           int(dart_wins * 6) + int(jackpot_wins * 64 / 4)

            total_could_win = int(basketball_rolls * 5 / 2) + int(dice_rolls * 6) + int(
                football_rolls * 5 / 3) + int(bowling_rolls * 6) + \
                              int(dart_rolls * 6) + int(jackpot_rolls * 64 / 4)

            lucky = int((total_points/total_could_win) * 100)

            total_games = user_data.get('total_games', 0)
            user_list.append(
                {'id': user_id, 'name': user_data.get('name', None), 'total_points': total_points,
                 'basketball_games': basketball_rolls, 'basketball_wins': basketball_wins, 'dice_games': dice_rolls, 'dice_wins': dice_wins,
                 'football_games': football_rolls, 'football_wins': football_wins, 'bowling_games': bowling_rolls, 'bowling_wins': bowling_wins,
                 'dart_games': dart_rolls, 'dart_wins': dart_wins,'jackpot_games': jackpot_rolls, 'jackpot_wins': jackpot_wins, 'lucky': lucky,
                 "total": total_games})

        sorted_user_list = sorted(user_list, key=lambda x: x['total_points'], reverse=True)
        message = ""
        for index, user in enumerate(sorted_user_list):
            if index == 0:
                message += f"🥇 {user['name']} ({user['total_points']} points of {user['total']} games) ({user['lucky']}% Lucky Ass)\n"
                message += f"🏀 = {user['basketball_wins']} of {user['basketball_games']}\n"
                message += f"🎯 = {user['dart_wins']} of {user['dart_games']}\n"
                message += f"🎳 = {user['bowling_wins']} of {user['bowling_games']}\n"
                message += f"⚽ = {user['football_wins']} of {user['football_games']}\n"
                message += f"🎰 = {user['jackpot_wins']} of {user['jackpot_games']}\n"
                message += f"🎲 = {user['dice_wins']} of {user['dice_games']}\n\n"
            elif index == 1:
                message += f"🥈 {user['name']} ({user['total_points']} points of {user['total']} games) ({user['lucky']}% Lucky Ass)\n"
                message += f"🏀 = {user['basketball_wins']} of {user['basketball_games']}\n"
                message += f"🎯 = {user['dart_wins']} of {user['dart_games']}\n"
                message += f"🎳 = {user['bowling_wins']} of {user['bowling_games']}\n"
                message += f"⚽ = {user['football_wins']} of {user['football_games']}\n"
                message += f"🎰 = {user['jackpot_wins']} of {user['jackpot_games']}\n"
                message += f"🎲 = {user['dice_wins']} of {user['dice_games']}\n\n"
            elif index == 2:
                message += f"🥉 {user['name']} ({user['total_points']} points of {user['total']} games) ({user['lucky']}% Lucky Ass)\n"
                message += f"🏀 = {user['basketball_wins']} of {user['basketball_games']}\n"
                message += f"🎯 = {user['dart_wins']} of {user['dart_games']}\n"
                message += f"🎳 = {user['bowling_wins']} of {user['bowling_games']}\n"
                message += f"⚽ = {user['football_wins']} of {user['football_games']}\n"
                message += f"🎰 = {user['jackpot_wins']} of {user['jackpot_games']}\n"
                message += f"🎲 = {user['dice_wins']} of {user['dice_games']}\n\n"
            else:
                message += f"🏅 {user['name']} ({user['total_points']} points of {user['total']} games) ({user['lucky']}% Lucky Ass)\n"
                message += f"🏀 = {user['basketball_wins']} of {user['basketball_games']}\n"
                message += f"🎯 = {user['dart_wins']} of {user['dart_games']}\n"
                message += f"🎳 = {user['bowling_wins']} of {user['bowling_games']}\n"
                message += f"⚽ = {user['football_wins']} of {user['football_games']}\n"
                message += f"🎰 = {user['jackpot_wins']} of {user['jackpot_games']}\n"
                message += f"🎲 = {user['dice_wins']} of {user['dice_games']}\n\n"

            # print(f"{rank_symbol} {user_name}: Basketball={basketball_score}, Dart={dart_score}, Total={total_score}")

        context.bot.send_message(chat_id=chat_id, text=message, parse_mode="Markdown")


def menu(update: Update, context: CallbackContext) -> None:
    global last_max_value_emoji, last_emoji_game, last_max_value_user_state, last_max_value_user

    chat_id = update.message.chat_id
    message_id = update.message.message_id

    if last_max_value_user[chat_id] is None:
        buttons = [
            [f'{last_emoji_game[chat_id]}'],
            ['LeaderBoard🏆', '🚑']]
        reply_markup = ReplyKeyboardMarkup(buttons, allow_sending_without_reply=True)
        message = update.message.reply_text(text=f'بوسیدم', reply_markup=reply_markup, allow_sending_without_reply=True)
        message.delete()
    else:
        buttons = [
            ['🎲', '🎰', '🎯'],
            ['🎳', '⚽️', '🏀'],
            ['LeaderBoard🏆']]
        # last_emoji = last_max_value_emoji[chat_id]
        last_winner_id = last_max_value_user[chat_id]
        last_winner_info = context.bot.get_chat_member(chat_id=chat_id, user_id=last_winner_id)
        last_winner_name = last_winner_info.user.first_name
        reply_markup = ReplyKeyboardMarkup(buttons, allow_sending_without_reply=True)
        update.message.reply_text(text=f'💋بوسیدم', reply_markup=reply_markup, allow_sending_without_reply=True)


def handle_last_game(update: Update, context: CallbackContext) -> None:
    global last_max_value_user_state, last_emoji_game

    chat_id = update.message.chat_id

    if last_max_value_user_state.get(chat_id) is None:
        context.bot.send_message(chat_id=chat_id, text="هنوز برنده ندازیم")
    else:
        last_winner_id = last_max_value_user_state[chat_id]
        last_winner_info = context.bot.get_chat_member(chat_id=chat_id, user_id=last_winner_id)
        last_winner_name = last_winner_info.user.first_name
        last_game = last_emoji_game.get(chat_id)
        mention = "[" + last_winner_name + "](tg://user?id=" + str(last_winner_id) + ")"
        response = f"آخرین برنده  {mention}بودس,\n اخرین بازی هم {last_game} بودس."
        context.bot.send_message(chat_id=chat_id, text=response, parse_mode="Markdown", disable_notification=True)


def handle_new_group(update: Update, context: CallbackContext) -> None:
    chat_id = update.message.chat.id
    GROUP_CHAT_IDS.append(chat_id)
    for member in update.message.new_chat_members:
        user_name = member.first_name
        user_id = member.id
        mention = "[" + user_name + "](tg://user?id=" + str(user_id) + ")"
        context.bot.send_message(chat_id=chat_id, text=f"Welcome {mention}!"
                                                       f" Here is some games to play:\n"
                                                       f"🎯 Dart\n"
                                                       f"🎳 Bowling\n"
                                                       f"⚽ Football\n"
                                                       f"🎲 Dice\n"
                                                       f"🏀 Basketball and\n"
                                                       f"🎰 Slot Machine\n"
                                                       f"Good Luck!!", parse_mode="Markdown")


def check_game_emoji_validity(update: Update, context: CallbackContext) -> bool:
    global last_max_value_user, last_max_value_time, last_emoji_game, last_user, emergency_user

    user_id = update.message.from_user.id
    user_name = update.message.from_user.first_name
    game_emoji = update.message.dice.emoji
    chat_id = update.message.chat.id

    if emergency_user[chat_id][user_id]['state'] is False or emergency_user[chat_id][user_id]['used'] >= 3:
        # emergency_user = None
        if chat_id in user_last_game_time.keys():
            if chat_id not in last_max_value_user.keys():
                last_max_value_user[chat_id] = None
            if game_emoji == '⚽' or game_emoji == '🏀' or game_emoji == '🎰':
                if (last_emoji_game[chat_id] != game_emoji or last_user[chat_id] == user_id) and last_max_value_user[
                    chat_id] != user_id:
                    update.message.delete()
                    mention = "[" + user_name + "](tg://user?id=" + str(user_id) + ")"
                    response = f"   {mention}  حادق باش دادا "
                    context.bot.sendMessage(chat_id=chat_id, text=response, parse_mode="Markdown")
                    return False
            else:
                if (last_emoji_game[chat_id] != game_emoji or last_user[chat_id] == user_id) and last_max_value_user[
                    chat_id] != user_id:
                    update.message.delete()
                    mention = "[" + user_name + "](tg://user?id=" + str(user_id) + ")"
                    response = f"   {mention}  نکون دادا "
                    context.bot.sendMessage(chat_id=chat_id, text=response, parse_mode="Markdown")
                    return False
    else:
        emergency_user[chat_id][user_id]['used'] += 1
        emergency_user[chat_id][user_id]['state'] = False
        if chat_id in user_last_game_time.keys():
            if chat_id not in last_max_value_user.keys():
                last_max_value_user[chat_id] = None
            if game_emoji == '⚽' or game_emoji == '🏀' or game_emoji == '🎰':
                if last_emoji_game[chat_id] != game_emoji:
                    update.message.delete()
                    mention = "[" + user_name + "](tg://user?id=" + str(user_id) + ")"
                    response = f"   {mention}  حادق باش دادا "
                    context.bot.sendMessage(chat_id=chat_id, text=response, parse_mode="Markdown")
                    return False
            else:
                if last_emoji_game[chat_id] != game_emoji:
                    update.message.delete()
                    mention = "[" + user_name + "](tg://user?id=" + str(user_id) + ")"
                    response = f"   {mention}  نکون دادا "
                    context.bot.sendMessage(chat_id=chat_id, text=response, parse_mode="Markdown")
                    return False
    user_last_game_time[chat_id] = {}
    user_last_game_time[chat_id][user_id] = datetime.now()
    last_emoji_game[chat_id] = game_emoji
    last_user[chat_id] = user_id
    if chat_id in last_max_value_user.keys() and last_max_value_user[chat_id] != user_id:
        if datetime.now() - last_max_value_time.get(chat_id, datetime.min) < timedelta(hours=1):
            update.message.delete()
            mention = "[" + user_name + "](tg://user?id=" + str(user_id) + ")"
            response = f"   {mention}  یخده چی صب کن دادا "
            context.bot.sendMessage(chat_id=chat_id, text=response, parse_mode="Markdown")
            return False

    return True


def handle_game_emoji(update: Update, context: CallbackContext) -> None:
    global last_max_value_user, last_max_value_time, user_max_value_count, last_emoji_game, GROUP_CHAT_IDS, last_user, user_last_game_time, last_max_value_user_state, last_max_value_emoji, leaderboard

    chat_id = update.message.chat.id
    user_id = update.message.from_user.id
    user_name = update.message.from_user.first_name
    game_emoji = update.message.dice.emoji
    game_value = update.message.dice.value

    max_values_f_b = {
        Dice.BASKETBALL: [4, 5],
        Dice.FOOTBALL: [3, 4, 5],
        Dice.SLOT_MACHINE: [64, 1, 22, 43],
    }
    max_values_other = {
        Dice.DARTS: 6,
        Dice.DICE: 6,
        Dice.BOWLING: 6,
    }

    if update.message.chat.id not in GROUP_CHAT_IDS:
        GROUP_CHAT_IDS.append(update.message.chat.id)

    if chat_id not in user_max_value_count.keys():
        user_max_value_count[chat_id] = {}
    if user_id not in user_max_value_count[chat_id].keys():
        user_max_value_count[chat_id][user_id] = 0

    if chat_id not in leaderboard.keys():
        leaderboard[chat_id] = {}
    if user_id not in leaderboard[chat_id].keys():
        leaderboard[chat_id][user_id] = {}

    if chat_id not in emergency_user.keys():
        emergency_user[chat_id] = {}
    if user_id not in emergency_user[chat_id].keys():
        emergency_user[chat_id][user_id] = {}
        emergency_user[chat_id][user_id]['state'] = False
        emergency_user[chat_id][user_id]['used'] = 0

    if not check_game_emoji_validity(update, context):
        return

    if leaderboard[chat_id][user_id].get('total_games') is None:
        leaderboard[chat_id][user_id]['total_games'] = 1
    else: leaderboard[chat_id][user_id]['total_games'] += 1

    if game_emoji == '🏀':
        if 'Basketball' not in leaderboard[chat_id][user_id].keys():
            leaderboard[chat_id][user_id]['Basketball'] = {}
        if leaderboard[chat_id][user_id]['Basketball'].get('total') is None:
            leaderboard[chat_id][user_id]['Basketball']['total'] = 1
        else:
            leaderboard[chat_id][user_id]['Basketball']['total'] += 1
    if game_emoji == '⚽':
        if 'Football' not in leaderboard[chat_id][user_id].keys():
            leaderboard[chat_id][user_id]['Football'] = {}
        if leaderboard[chat_id][user_id]['Football'].get('total') is None:
            leaderboard[chat_id][user_id]['Football']['total'] = 1
        else:
            leaderboard[chat_id][user_id]['Football']['total'] += 1
    if game_emoji == '🎰':
        if 'Jackpot' not in leaderboard[chat_id][user_id].keys():
            leaderboard[chat_id][user_id]['Jackpot'] = {}
        if leaderboard[chat_id][user_id]['Jackpot'].get('total') is None:
            leaderboard[chat_id][user_id]['Jackpot']['total'] = 1
        else:
            leaderboard[chat_id][user_id]['Jackpot']['total'] += 1
    if game_emoji == '🎲':
        if 'Dice' not in leaderboard[chat_id][user_id].keys():
            leaderboard[chat_id][user_id]['Dice'] = {}
        if leaderboard[chat_id][user_id]['Dice'].get('total') is None:
            leaderboard[chat_id][user_id]['Dice']['total'] = 1
        else:
            leaderboard[chat_id][user_id]['Dice']['total'] += 1
    if game_emoji == '🎯':
        if 'Dart' not in leaderboard[chat_id][user_id].keys():
            leaderboard[chat_id][user_id]['Dart'] = {}
        if leaderboard[chat_id][user_id]['Dart'].get('total') is None:
            leaderboard[chat_id][user_id]['Dart']['total'] = 1
        else:
            leaderboard[chat_id][user_id]['Dart']['total'] += 1
    if game_emoji == '🎳':
        if 'Bowling' not in leaderboard[chat_id][user_id].keys():
            leaderboard[chat_id][user_id]['Bowling'] = {}
        if leaderboard[chat_id][user_id]['Bowling'].get('total') is None:
            leaderboard[chat_id][user_id]['Bowling']['total'] = 1
        else:
            leaderboard[chat_id][user_id]['Bowling']['total'] += 1


    if game_emoji == '⚽' or game_emoji == '🏀' or game_emoji == '🎰':
        leaderboard[chat_id][user_id]['name'] = user_name
        if game_value in max_values_f_b[game_emoji]:
            if game_emoji == '⚽':
                if leaderboard[chat_id][user_id]['Football'].get('win') is None:
                    leaderboard[chat_id][user_id]['Football']['win'] = 1
                else:
                    leaderboard[chat_id][user_id]['Football']['win'] += 1
            if game_emoji == '🏀':
                if leaderboard[chat_id][user_id]['Basketball'].get('win') is None:
                    leaderboard[chat_id][user_id]['Basketball']['win'] = 1
                else:
                    leaderboard[chat_id][user_id]['Basketball']['win'] += 1
            if game_emoji == '🎰':
                if leaderboard[chat_id][user_id]['Jackpot'].get('win') is None:
                    leaderboard[chat_id][user_id]['Jackpot']['win'] = 1
                else:
                    leaderboard[chat_id][user_id]['Jackpot']['win'] += 1
            user_max_value_count[chat_id][user_id] += 1
            last_max_value_user[chat_id] = user_id
            last_max_value_user_state[chat_id] = user_id
            last_max_value_emoji[chat_id] = game_emoji
            context.user_data['last_emoji'] = game_emoji
            last_max_value_time[chat_id] = datetime.now()
            if game_value == 1:
                update.message.reply_text("عرق")
            if game_value == 64:
                update.message.reply_text("🍑سفیدی")
            if user_max_value_count[chat_id][user_id] == 2:
                update.message.reply_text("قشنگ شد!")
            if user_max_value_count[chat_id][user_id] == 3:
                update.message.reply_text("باریکلا!")
            if user_max_value_count[chat_id][user_id] == 4:
                update.message.reply_text("بنازم ضرب شست!")
            if user_max_value_count[chat_id][user_id] == 5:
                update.message.reply_text("عاچدهست حالا!")
                user_max_value_count[chat_id][user_id] = 0
                # record = game_records.find_one({'chat_id': chat_id, 'user_id': user_id, 'game': game_emoji})
                # if record:
                #     game_records.update_one({'_id': record['_id']}, {'$inc': {'wins': 1}})
                # else:
                #     game_records.insert_one(
                #         {'chat_id': chat_id, 'user_id': user_id, 'user_name': user_name, 'game': game_emoji, 'wins': 1})
        else:
            last_winner = last_max_value_user[chat_id]
            if user_id == last_winner:
                del user_max_value_count[chat_id]
                last_max_value_user[chat_id] = None

                del last_max_value_time[chat_id]
    else:
        leaderboard[chat_id][user_id]['name'] = user_name
        if game_value == max_values_other[game_emoji]:
            if game_emoji == '🎯':
                if leaderboard[chat_id][user_id]['Dart'].get('win') is None:
                    leaderboard[chat_id][user_id]['Dart']['win'] = 1
                else:
                    leaderboard[chat_id][user_id]['Dart']['win'] += 1
            if game_emoji == '🎳':
                if leaderboard[chat_id][user_id]['Bowling'].get('win') is None:
                    leaderboard[chat_id][user_id]['Bowling']['win'] = 1
                else:
                    leaderboard[chat_id][user_id]['Bowling']['win'] += 1
            if game_emoji == '🎲':
                if leaderboard[chat_id][user_id]['Dice'].get('win') is None:
                    leaderboard[chat_id][user_id]['Dice']['win'] = 1
                else:
                    leaderboard[chat_id][user_id]['Dice']['win'] += 1
            user_max_value_count[chat_id][user_id] += 1
            last_max_value_user[chat_id] = user_id
            last_max_value_user_state[chat_id] = user_id
            last_max_value_emoji[chat_id] = game_emoji
            context.user_data['last_emoji'] = game_emoji
            last_max_value_time[chat_id] = datetime.now()
            if user_max_value_count[chat_id][user_id] == 2:
                update.message.reply_text("قشنگ شد")
            if user_max_value_count[chat_id][user_id] == 3:
                update.message.reply_text("باریکلا")
            if user_max_value_count[chat_id][user_id] == 4:
                update.message.reply_text("بنازم ضرب شست")
            if user_max_value_count[chat_id][user_id] == 5:
                update.message.reply_text("عاچدهست حالا")
                user_max_value_count[chat_id][user_id] = 0
                # record = game_records.find_one({'chat_id': chat_id, 'user_id': user_id, 'game': game_emoji})
                # if record:
                #     game_records.update_one({'_id': record['_id']}, {'$inc': {'wins': 1}})
                # else:
                #     game_records.insert_one(
                #         {'chat_id': chat_id, 'user_id': user_id, 'user_name': user_name, 'game': game_emoji, 'wins': 1})
        else:
            last_winner = last_max_value_user[chat_id]
            if user_id == last_winner:
                del user_max_value_count[chat_id]
                last_max_value_user[chat_id] = None
                del last_max_value_time[chat_id]
    menu(update, context)

    # user_record = game_records.find_one({'chat_id': chat_id, 'user_id': user_id})
    # if user_record:
    #     game_records.update_one({'_id': user_record['_id']}, {'$set': {'last_game_time': datetime.now()}})
    # else:
    #     game_records.insert_one(
    #         {'chat_id': chat_id, 'user_id': user_id, 'user_name': user_name, 'last_game_time': datetime.now()})
    #
    # if chat_id in GROUP_CHAT_IDS:
    #     if last_user[chat_id] != user_id or (datetime.now() - user_last_game_time[chat_id]).seconds >= 60:
    #         if last_user[chat_id] != user_id:
    #             last_user[chat_id] = user_id
    #         user_last_game_time[chat_id] = datetime.now()
    #         update.message.reply_text(f"{user_name} توی بازی {game_emoji} {game_value} گرفت!")


def error(update: Update, context: CallbackContext) -> None:
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main() -> None:
    global last_emoji_game, user_last_game_time, last_max_value_time, emergency_user, context
    updater = Updater(TOKEN)

    dp = updater.dispatcher

    dp.add_handler(CommandHandler('gameplay', menu))
    dp.add_handler(CommandHandler('game', handle_last_game))

    game_emoji_filter = Filters.regex(r'[\U0001F3AF\U0001F3B2\U0001F3B3\U0001F3C0\U0001F3C8\U0001F3B0]')

    dp.add_handler(MessageHandler(Filters.dice | game_emoji_filter, handle_game_emoji))
    dp.add_handler(MessageHandler(Filters.status_update.new_chat_members, handle_new_group))
    dp.add_handler(MessageHandler(Filters.text, text_handler))

    dp.add_error_handler(error)

    schedule.every().day.at("00:00").do(reset_emergency, context=dp)
    updater.start_polling()
    while True:
        schedule.run_pending()
        time.sleep(1)
    # updater.idle()


if __name__ == '__main__':
    main()
